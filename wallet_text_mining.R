###text mining -> clustering

rawData <- read.csv('wallet_total_transactions_170715.csv')

### segmenting for Argentina
names(rawData)
data_arg <- subset(rawData,Site=='MLA')

## subset de data a analizar
data <- data_arg

## segmenting for F&F
data$is_FF <- is.na(data$X..Collector.FEE) & (data$Operation.Type == "Money Transfer")
data <- subset(data,is_FF=="TRUE")
data[1:100,1:10]

data$Payment.Reason <- as.character(data$Payment.Reason)
payer_reasons <- aggregate(Payment.Reason~Payer_username,data,FUN=paste,collapse=' ')
payer_reasons[1:500,]

# The collapse argument in paste tells R that we want to paste together elements of a vector, rather than pasting vectors together.
#data_reasons

### adjusting the encoding type
data_reasons <- paste(payer_reasons$Payment.Reason)
data_reasons <- iconv(data_reasons,"WINDOWS-1252","UTF-8")

### creating the vector source and corpus
library(tm)
reasons_source <- VectorSource(data_reasons)
#reasons_source
reasonsCorpus <- Corpus(reasons_source)
#reasonsCorpus
#inspect(reasonsCorpus)

library(SnowballC)
paymentReasons <- tm_map(reasonsCorpus, content_transformer(tolower))
paymentReasons <- tm_map(paymentReasons, removePunctuation) 
paymentReasons <- tm_map(paymentReasons, removeNumbers) 
paymentReasons <- tm_map(paymentReasons, stripWhitespace)

inspect(paymentReasons)

ownStopWords <- as.character(c('y',"que","el","le","i","x","de","su","mi","vos","la","te","en","por","del","va","con","las","lo","los","les","para","su","es","a","o","una","unos","unas","un","tus","mis","como","tal","saludo","saludos","cordiales","cordial","hola","tardes","buenas","muchas","gracias","aca","mucha","gracias","dinero","envio","solicitud","mercadopago","traves"))
ownStopWords
paymentReasons <- tm_map(paymentReasons,removeWords, c(stopwords("spanish"),ownStopWords))

#paymentReasons <- tm_map(paymentReasons,removeWords, stopwords("portuguese"), , lazy=TRUE)

#Stem
paymentReasons <- tm_map(paymentReasons, stemDocument)

#inspect(paymentReasons)
#paymentReasons
 
###
BigramTokenizer <- function(x)
    unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
dtm <- DocumentTermMatrix(paymentReasons, control = list(tokenize = BigramTokenizer))

#dtm <- DocumentTermMatrix(paymentReasons)
dtm <- removeSparseTerms(dtm, 0.9995)
dtm
inspect(dtm)[1:100,]

dtm_matrix <- as.matrix(dtm)
#dtm_matrix2 <- dtm_matrix[which(rowSums(dtm_matrix) > 0),] 
dim(dtm_matrix)
dtm_matrix[1:10,1:30]

#frequency <- colSums(dtm_matrix)
#frequency <- sort(frequency, decreasing=TRUE)
#head(frequency)
#frequency[1:50]

## topic modeling

library(textir) # to get the data
library(maptpx) # for the topics function

#  upon what scale to measure `words' for k-means is unclear (the answer is
#  actually: don't use kmeans; use a multinomial mixture instead).
#  You could do log(1+f) instead, or something like tf-idf (google that)
#  but in absence of better guidance, I just look at scaled f

#dtm_matrix[1:10,]
rowSums(dtm_matrix)
weight_matrix <- as.matrix(dtm_matrix/rowSums(dtm_matrix))
dim(weight_matrix)

##if we want to eliminate all-zeros rows (TO-DO: podemos matchear despues cada row con el payments del dataset original?)
weight_matrix[is.na(weight_matrix)] <- 0

# takes time, because you're making it dense from sparse
fs <- scale(weight_matrix)
dim(fs)

kmfs <- kmeans(fs,5) 
summary(kmfs) 
table(kmfs$cluster)

## both this and topic modelling takes a long time...
## you're fitting massively high dimensional models (K*ncol(x))
## there are approximate distributed algorithms out there
## `stochastic block descent', etc...
## for really big data, I think you just focus on subsamples, most common words, etc.
## all these methods find are the dominant sources of variation, 
## so those should be present in small subsamples

## interpretation: we can see the words with cluster centers
## highest above zero (these are in units of standard deviation of f)
print(apply(kmfs$centers,1,function(c) colnames(fs)[order(-c)[1:10]]))
## shows words that occur far more in these clusters than on average

## topic modelling.  Treat counts as actual counts!
## i.e., model them with a multinomial
## we'll use the topics function in maptpx (there are other options out there)

## you need to convert from a Matrix to a `slam' simple_triplet_matrix
## luckily, this is easy.
x <- as.simple_triplet_matrix(dtm_matrix)

# to fit, just give it the counts, number of `topics' K, and any other args
tpc <- topics(x,K=5,tol=10) 
# note: tol is `tolerance threshold on minimization of deviance'
# the default is small, and not dependent upon sample size or dimension, 
# so it can lead to topics optimization taking forever.  
# You'll get a fine rough answer at higher tolerances.
rownames(tpc$theta)[order(tpc$theta[,1], decreasing=TRUE)[1:10]]
rownames(tpc$theta)[order(tpc$theta[,2], decreasing=TRUE)[1:10]]
rownames(tpc$theta)[order(tpc$theta[,3], decreasing=TRUE)[1:10]]
rownames(tpc$theta)[order(tpc$theta[,4], decreasing=TRUE)[1:10]]


## choosing the number of topics
## If you supply a vector of topic sizes, it uses a Bayes factor to choose
## (BF is like exp(-BIC), so you choose the bigggest BF)
## the algo stops if BF drops twice in a row
tpcs <- topics(x,K=5*(1:5),tol=10) # it chooses 10 topics 

## interpretation
# summary prints the top `n' words for each topic,
# under ordering by `topic over aggregate' lift:
#    the topic word prob over marginal word prob.
summary(tpcs, n=5) 
summary(tpc, n=5) 

# this will promote rare words that with high in-topic prob

# alternatively, you can look at words ordered by simple in-topic prob
## the topic-term probability matrix is called 'theta', 
## and each column is a topic
## we can use these to rank terms by probability within topics
rownames(tpcs$theta)[order(tpcs$theta[,1], decreasing=TRUE)[1:10]]
rownames(tpcs$theta)[order(tpcs$theta[,2], decreasing=TRUE)[1:10]]
names(tpcs)


## Wordles!
## use the wordcloud library to plot a few
library(wordcloud)
## we'll size the word proportional to its in-topic probability
## and only show those with > 0.004 omega
## (it will still likely warn that it couldn't fit everything)
par(mfrow=c(4,3))
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,1], min.freq=0.004, col="maroon")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,2], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,3], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,4], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,5], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,6], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,7], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,8], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,9], min.freq=0.004, col="navy")
wordcloud(row.names(tpcs$theta), 
	freq=tpcs$theta[,10], min.freq=0.004, col="navy")

## interpret the relationship between topics and overall rating
library(gamlr)
## omega is the n x K matrix of document topic weights
## i.e., how much of each doc is from each topic
## we'll regress overall rating onto it
stars <- we8thereRatings[,"Overall"]
tpcreg <- gamlr(tpcs$omega, stars)
# number of stars up or down for moving up 10\% weight in that topic
drop(coef(tpcreg))*0.1

regtopics.cv <- cv.gamlr(tpcs$omega, stars)
## give it the word %s as inputs
x <- 100*we8thereCounts/rowSums(we8thereCounts)
regwords.cv <- cv.gamlr(x, stars)

par(mfrow=c(1,2))
plot(regtopics.cv)
mtext("topic regression", font=2, line=2)
plot(regwords.cv)
mtext("bigram regression", font=2, line=2)

# max OOS R^2s
max(1-regtopics.cv$cvm/regtopics.cv$cvm[1])
max(1-regwords.cv$cvm/regwords.cv$cvm[1])

####### previous functions research### example of word count using tm
library("tm") # version 0.6, you seem to be using an older version
data(crude)
revs <- tm_map(crude, content_transformer(tolower)) 
revs <- tm_map(revs, removeWords, stopwords("english")) 
revs <- tm_map(revs, removePunctuation) 
revs <- tm_map(revs, removeNumbers) 
revs <- tm_map(revs, stripWhitespace) 
dtm <- DocumentTermMatrix(revs)
rowSums(as.matrix(dtm))

####
library("RWeka")
library("tm")

data("crude")

BigramTokenizer <- function(x) NGramTokenizer(x, Weka_control(min = 2, max = 2))
tdm <- TermDocumentMatrix(crude, control = list(tokenize = BigramTokenizer))

inspect(tdm[340:345,1:10])

library("tm")
data("crude")
BigramTokenizer <- function(x)
    unlist(lapply(ngrams(words(x), 2), paste, collapse = " "), use.names = FALSE)
tdm <- TermDocumentMatrix(crude, control = list(tokenize = BigramTokenizer))
inspect(removeSparseTerms(tdm[, 1:10], 0.7))



#####
data("crude")
tdm <- TermDocumentMatrix(crude,
                          control = list(removePunctuation = TRUE,
                                         stopwords = TRUE))
dtm <- DocumentTermMatrix(crude,
                          control = list(weighting =
                                         function(x)
                                         weightTfIdf(x, normalize =
                                                     FALSE),
                                         stopwords = TRUE))
inspect(tdm[155:160,1:5])
inspect(tdm[c("price", "texas"),c("127","144","191","194")])
inspect(dtm[1:5,155:160])

#####
data("crude") 
## Document access triggers the stemming function 
## (i.e., all other documents are not stemmed yet) 
tm_map(crude, stemDocument, lazy = TRUE)[[1]] 
## Use wrapper to apply character processing function 
tm_map(crude, content_transformer(tolower)) 
## Generate a custom transformation function which takes the heading as new content 
headings <- function(x) PlainTextDocument(meta(x, "heading"), id = meta(x, "id"), language = meta(x, "language")) 
inspect(tm_map(crude, headings))

data("crude") 
tdm <- TermDocumentMatrix(crude) 
tdm
inspect(tdm)
tdm <- removeSparseTerms(tdm, 0.96)

data("crude") 
stemCompletion(c("compan", "entit", "suppl"), crude)

Examples 
library('SnowballC')
data("crude") 
termFreq(crude[[14]]) 
strsplit_space_tokenizer <- function(x) 
	unlist(strsplit(as.character(x), "[[:space:]]+"))
strsplit_space_tokenizer 
ctrl <- list(tokenize = strsplit_space_tokenizer, 
	removePunctuation = list(preserve_intra_word_dashes = TRUE), 
	stopwords = c("reuter", "that"), 
	stemming = TRUE, 
	wordLengths = c(4, Inf))
ctrl 
termFreq(crude[[14]], control = ctrl)

data("crude") 
inspect(crude)
MC_tokenizer(crude[[2]]) 
scan_tokenizer(crude[[1]]) 
strsplit_space_tokenizer <- function(x) 
	unlist(strsplit(as.character(x), "[[:space:]]+")) 
strsplit_space_tokenizer(crude[[1]])

####
data(SOTU) 
corp <- SOTU 
corp <- tm_map(corp, removePunctuation) 
corp <- tm_map(corp, content_transformer(tolower)) 
corp <- tm_map(corp, removeNumbers) 
corp <- tm_map(corp, function(x)removeWords(x,stopwords())) 
term.matrix <- TermDocumentMatrix(corp) 
term.matrix <- as.matrix(term.matrix) 
colnames(term.matrix) <- c("SOTU 2010","SOTU 2011") comparison.cloud(term.matrix,max.words=40,random.order=FALSE) commonality.cloud(term.matrix,max.words=40,random.order=FALSE)

###aggregating by columns -> other methods

data %>% group_by(period,Payer) %>% arrange(period, Payer) %>% summarise(TPV = sum(TPV) %>% ungroup() )



class(data)
library(data.table)
data <- data.table(data)
data[, sum(TPV), by = list(Payer, period)]
names(data)

as.data.frame(t(
   sapply(split(data, data$period), function(x) {

      # for each x - data frame subset such grouped by year
      apply(x, 2, function(y) { 

          # for each y, i.e. column in x
          if class(y) == numeric {
          	sum(y)
          	} else {
          	t <- table(y)
          	names(t)[which.max(t)]  # return the label that occurs most often
          	}
      })

   })
))

#
library(dplyr)
mtcars[1:10,]
summarise(mtcars, mean(disp))
group_by(mtcars, cyl)[1:10,] 
summarise(group_by(mtcars, cyl), mean(disp)) 
summarise(group_by(mtcars, cyl), m = mean(disp), sd = sd(disp)) 
#With data frames, you can create and immediately use summaries 
by_cyl <- mtcars %>% group_by(cyl) 
by_cyl %>% summarise(a = n(), b = a + 1)

data$period

data_payer <- group_by(data,Payer)
data_payer
data_period_payer <- group_by(data_period,Payer)
data_period_payer[1:10]

summarise(data_period,TPV=sum(TPV),TPN=sum(TPN))

summarise

library(tidyr)
gather(data,period,Payer)

library(plyr)

ddply(data_period, .(Payer), summarise, 
      TPV = sum(TPV), 
      TPN = sum(TPN))



