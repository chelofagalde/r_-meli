
create multiset volatile table SELLERS_ULT6M AS
(
SELECT sit_site_id_cus, cus_cust_id,oreplace(cus_nickname,',',' ') CUS_NICKNAME , cus_email, CUS_MP_CLASS_PRO_FLAG
FROM LK_CUS_CUSTOMERS_DATA t1
WHERE sit_site_id_cus = 'MLB' 
and (CUS_MP_LAST_SALE_OFF_ML >= '2014-12-19 00:00:00' or CUS_MP_LAST_SALE_ON_ML >= '2014-12-19 00:00:00')
and cus_cust_status not in ('I','T')
) with data primary index (cus_cust_id) on commit preserve rows;


create multiset volatile table PAGOS AS
(
SELECT t1.sit_site_id_cus, t1.cus_cust_id, t2.pay_payment_id, t2.pay_total_paid_amt, t2.pay_created_datetime, t2.pay_ope_mlibre_flag
FROM sellers_ult6m t1
inner join bt_mp_pay_payments t2
on t1.cus_cust_id = t2.cus_cust_id_sel
where t2.pay_status_id in ('approved','refunded','in_mediation','charged_back')
and t2.pay_operation_type_id in ('regular_payment','payment_addition','money_transfer','recurring_payment')
) with data primary index (cus_cust_id,pay_payment_id) on commit preserve rows;


create multiset volatile table INFO_USR AS
(
SELECT 
sit_site_id_cus, cus_cust_id, 
sum(case when pay_created_datetime >= '2015-03-19 00:00:00' then pay_total_paid_amt else 0 end) TPV_ULT3M,
sum(case when pay_ope_mlibre_flag = 'N' then 1 else 0 end) TPN_OFF, sum(case when pay_ope_mlibre_flag = 'Y' then 1 else 0 end) TPN_ON
from PAGOS t1
group by 1,2
) with data primary index (cus_cust_id) on commit preserve rows;



-- Base churn
select t2.cus_cust_id,t2.cus_nickname, t2.cus_email, t2.CUS_MP_CLASS_PRO_FLAG
from INFO_USR t1
inner join SELLERS_ULT6M t2
on t1.cus_cust_id = t2.cus_cust_id
where TPV_ULT3M < 300 and (TPN_ON > 0 or TPN_OFF > 0)

-- Sellers OFF
select t2.cus_cust_id,t2.cus_nickname, t2.cus_email  ,t2.CUS_MP_CLASS_PRO_FLAG
from INFO_USR t1
inner join SELLERS_ULT6M t2
on t1.cus_cust_id = t2.cus_cust_id
where TPV_ULT3M >= 300 and TPN_ON = 0 and TPN_OFF > 0

-- Sellers ON/OFF
select t2.cus_cust_id,t2.cus_nickname, t2.cus_email  ,t2.CUS_MP_CLASS_PRO_FLAG
from INFO_USR t1
inner join SELLERS_ULT6M t2
on t1.cus_cust_id = t2.cus_cust_id
where TPV_ULT3M >= 300 and TPN_ON > 0 and TPN_OFF >= 0





create multiset volatile table SELLERS_SERVICIOS AS
(
SELECT cus_cust_id_sel, count(ite_item_id) PUBS
FROM LK_ITE_ITEMS_PH t1
INNER JOIN AG_LK_CAT_CATEGORIES_PH t2
ON	t1.SIT_SITE_ID = t2.sit_site_id
and t2.CAT_CATEG_ID_L7 =t1.CAT_CATEG_ID       
WHERE t1.photo_id = 'TODATE'
AND t2.photo_id = 'TODATE'
AND cast(t1.ite_auction_start_datetime as date)  >= 1141219
AND t1.sit_site_id = 'MLB'
AND t2.CAT_CATEG_ID_L1 = '1540'
GROUP BY 1
)
WITH DATA
ON COMMIT PRESERVE ROWS


select cus_cust_id,oreplace(cus_nickname,',',' ') CUS_NICKNAME , cus_email, CUS_MP_CLASS_PRO_FLAG, PUBS
from SELLERS_SERVICIOS t1
inner join LK_CUS_CUSTOMERS_DATA t2
on t1.cus_cust_id_sel = t2.cus_cust_id
where t2.cus_cust_status not in ('I','T')
